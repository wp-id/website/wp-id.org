<?php
/**
 * Extra WP config
 */

global $redis_server;

$cache_host = getenv( 'WP_CACHE_HOST' );
$cache_port = getenv( 'WP_CACHE_PORT' );

if ( $cache_host && $cache_port ) {
	$redis_server = [
		'host' => $cache_host,
		'port' => (int) $cache_port,
	];
}

foreach ( [ 'WP_DEBUG', 'WP_DEBUG_LOG', 'SCRIPT_DEBUG' ] as $key ) {
	if ( getenv( $key ) ) {
		define( $key, true );
	}
}
