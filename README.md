# WP-ID.org v3

https://wp-id.org

View README in [English](README-en.md).

## Prasyarat

Pastikan anda telah memasang [Docker](https://docs.docker.com/engine/installation/) dan [Docker Compose](https://docs.docker.com/compose/) dan anda telah berhasil menjalankan `docker run hello-world`:

## Penggunaan

Jika anda ingin membantu kami (terima kasih sebelumnya!):

- Clone repo ini lalu masuk ke direktori:
  ```sh
  git clone https://gitlab.com/wp-id/website/wp-id.org.git wp-id.org
  cd wp-id.org
  ```
- Tambahkan `127.0.0.1 local.wp-id.org` ke `/etc/hosts`
- Jalankan skrip penyiapan:
  ```sh
  ./bin/setup-local
  ```
- Jalankan container:
  ```sh
  docker compose up
  ```
- Untuk menghentikan container:
  ```sh
  docker compose down
  ```
- Untuk menjalankan WP CLI:
  ```sh
  docker compose exec -u www-data backend wp <command>
  ```
- Untuk mengimpor basis data:
  ```sh
  docker compose exec -u www-data -T backend wp db import - < PATH_TO_SQL_DUMP_FILE
  ```
- Untuk mengekspor basis data:
  ```sh
  docker compose exec -u www-data -T backend wp db export - > PATH_TO_SQL_DUMP_FILE
  ```
